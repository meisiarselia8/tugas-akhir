    <nav class="navbar navbar-expand-lg navbar-light navbar-puskesmas fixed-top navbar-fixed-top" data-aos="fade-down">
        <div class="container">
            <a href="/index.html" class="navbar-brand">
                <img src="/images/logo.svg" alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="/index.html" class="nav-link">Beranda</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="pelayananDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Pelayanan
                        </a>
                        <div class="dropdown-menu" aria-labelledby="pelayananDropdown">
                            <a class="dropdown-item" href="/jadwal.html">Jadwal & Jenis Pelayanan</a>
                            <a class="dropdown-item" href="/kritik-saran.html">Kritik & Saran</a>
                            <a class="dropdown-item" href="/pendaftaran-online.html">Pendaftaran Online</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/tentang.html" class="nav-link">Tentang</a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="infoDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Info Kesehatan
                        </a>
                        <div class="dropdown-menu" aria-labelledby="infoDropdown">
                            <a class="dropdown-item active" href="/berita.html">Berita</a>
                            <a class="dropdown-item" href="/faq.html">FAQ</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/kontak.html" class="nav-link">Kontak</a>
                    </li>
                    <li class="nav-item">
                        <a href="/login.html" class="btn btn-success nav-link px-4 text-white">Masuk
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
