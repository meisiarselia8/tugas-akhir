@extends('layouts.app')

@section('title')
Detail Artikel Page
@endsection

@section('content')
<div class="page-content page-details">
    <section class="artikel-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('home') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active"></li>
                            Detail Artikel
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <div class="artikel-details-container" data-aos="fade-up">
        <section class="artikel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 mt-4">
                        <h1>4 Efek Narkoba yang Berbahaya untuk Kesehatan Jantung</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="artikel-description">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <p>
                            Obat-obatan terlarang, seperti narkoba, mempunyai efek
                            berbahaya untuk kesehatan jantung. Hipertensi, jantung
                            berdetak cepat, serangan jantung, hingga kematian adalah
                            beberapa di antaranya. Apa saja efek berbahaya bagi jantung
                            dari kecanduan narkoba? Simak penjelasan lengkapnya di sini.
                        </p>
                        <h3>Macam-macam efek narkoba pada jantung</h3>
                        <p>
                            Narkoba adalah obat terlarang yang mengandung zat adiktif,
                            sehingga membuat seseorang kecanduan. Beberapa contoh narkoba,
                            antara lain heroin, kokain, sabu, atau ganja.
                        </p>
                        <p>
                            Banyak efek negatif narkoba pada tubuh, salah satunya adalah
                            masalah kesehatan jantung. Setiap jenisnya bisa mendatangkan
                            dampak yang berbeda-beda. Berikut ini adalah beberapa efek
                            narkoba pada jantung:
                        </p>
                        <h5>1. Efek kakoin pada jantung</h5>
                        <p>
                            Kokain adalah zat stimulan kuat dan adiktif yang umumnya
                            berbentuk bubuk. Jenis narkoba ini diklaim mempunyai efek yang
                            paling merusak jantung. Kontraksi pada otot jantung adalah
                            salah satu jenis kerja saraf simpatik.
                        </p>
                        <p>
                            Studi dalam The Cardiovascular Effect of Cocaine
                            mengungkapkan, kokain memengaruhi sistem saraf simpatik.
                        </p>
                        <p>
                            Aktivitas saraf simpatik yang berlebihan ini akan berdampak
                            pada sistem kardiovaskuler. Akibatnya, terjadi penyempitan
                            pembuluh darah kapiler yang mengurangi aliran darah ke otot
                            jantung.
                        </p>
                        <p>
                            Beberapa risiko kesehatan yang berhubungan dengan jantung saat
                            mengonsumsi kokain, antara lain:
                        </p>
                        <h6>&gt; Penggumpalan darah</h6>
                        <h6>&gt; Serangan jantung</h6>
                        <h6>&gt; Penyakit jantung koroner</h6>
                        <br />
                        <h5>2. Efek opioid pada jantung</h5>
                        <p>
                            Opioid adalah jenis narkoba yang bisa dijadikan sebagai obat
                            pereda nyeri tingkat tinggi. Beberapa contoh obat golongan
                            opioid, antara lain heroin, morfin, kodein, metadon, fentanyl,
                            dan oksikodon.
                        </p>
                        <p>
                            Penggunaan opioid dalam dunia kesehatan diperbolehkan tapi
                            harus dalam pengawasan dokter. Sebab, penyalahgunaan opioid
                            bisa menyebabkan overdosis dan mengakibatkan efek berbahaya
                            pada jantung, seperti:
                        </p>
                        <h6>&gt; Gagal jantung</h6>
                        <h6>&gt; Bradikardia</h6>
                        <h6>&gt; Endokarditis</h6>
                        <br />
                        <h5>3. Efek metamfetamin pada jantung</h5>
                        <p>
                            Metamfetamin (meth) adalah jenis narkoba paling berbahaya dan
                            membuat penggunanya langsung kecanduan.
                        </p>
                        <p>Di Indonesia, meth biasa disebut sebagai sabu-sabu.</p>
                        <p>
                            Mengutip American Heart Association, metamfetamin diketahui
                            dapat menyebabkan pembuluh darah menyempit, tekanan darah
                            meningkat, dan memengaruhi sinyal listrik jantung.
                        </p>
                        <p>
                            Hal tersebut meningkatkan risiko komplikasi pada jantung,
                            seperti:
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection

:@push('addon-script')
<script src="/vendor/vue/vue.js"></script>
<script>
    var gallery = new Vue({
        el: "#gallery",
        mounted() {
            AOS.init();
        },
        data: {
            activePhoto: 0,
            photos: [{
                    id: 1,
                    url: "/images/artikel-detail-1.jpg",
                },
                {
                    id: 2,
                    url: "/images/artikel-detail-2.jpg",
                },
                {
                    id: 3,
                    url: "/images/artikel-detail-3.jpg",
                },
                {
                    id: 4,
                    url: "/images/artikel-detail-4.jpg",
                },
            ],
        },
        methods: {
            changeActive(id) {
                this.activePhoto = id;
            },
        },
    });

</script>
@endpush
