@extends('layouts.login')

@section('title')
Register Page
@endsection

@section('content')
<div class="page-content page-auth" id="register">
    <div class="section-puskesmas-auth" data-aos="fade-up">
        <div class="container">
            <div class="row align-items-center justify-content-center row-login">
                <div class="col-lg-4" style="margin-top: 120px">
                    <h2>Registrasi Akun</h2>
                    <form class="mt-3">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" class="form-control is-valid" v-model="name" autofocus />
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control is-invalid " v-model="email" />
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control " />
                        </div>

                        <div class="text-center">
                            <a href="/dashboard.html" class="btn btn-success btn-block mt-3 " style="width: 100%;">
                                Sign In Up Now
                            </a><br>
                            <a href="{{ route('login') }}" class="btn btn-signup btn-block  mt-3" style="width: 100%;">
                                Back to Sign In
                            </a>
                        </div>
                        <div class="form-group">
                            <p class="text-muted">
                                <i><span class="text-danger">*Nb: </span></i>Anda dapat
                                melakukan pendaftaran antrian online setelah anda melakukan
                                registrasi akun
                            </p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('addon-style')
<style>
    .btn-signup:hover {
        background-color: #ddd;
    }

</style>
@endpush
