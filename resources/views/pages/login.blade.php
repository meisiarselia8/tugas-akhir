@extends('layouts.login')

@section('title')
Login Page
@endsection

@section('content')
<div class="page-content page-auth">
    <div class="section-puskesmas-auth" data-aos="fade-up">
        <div class="container">
            <div class="row align-items-center row-login">
                <div class="col-lg-6 text-center">
                    <img src="/img/login-placeholder.png" alt="" class="w-50 mb-4 mb-lg-none" />
                </div>
                <div class="col-lg-5" style="margin-top: 150px">
                    <h2>
                        Daftar Antrian,<br />
                        menjadi lebih mudah
                    </h2>

                    <form action="" class="mt-3">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control w-75" />
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control w-75" />
                        </div>
                        <a href="/dashboard.html" class="btn btn-success btn-block w-75 mt-4">
                            Sign In to My Account
                        </a>
                        <a href="{{ route('register') }}" class="btn btn-signup btn-block w-75 mt-2">
                            Sign Up
                        </a>
                        <div class="form-group">
                            <p class="text-muted">
                                <i><span class="text-danger">*Nb: </span></i>Anda dapat
                                melakukan pendaftaran <br />
                                online setelah anda melakukan login
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('addon-style')
<style>
    .btn-signup:hover {
        background-color: #ddd;
    }

</style>
@endpush
